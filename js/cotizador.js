function calcular(){
    let valor = document.getElementById('valorAuto').value;
    let planCredito = document.getElementById('plan').value;
    let enganche = document.getElementById('pEnganche');
    let totalF = document.getElementById('pTotalFinanciar');
    let pagoMensu = document.getElementById('pagoMensual');
    
    enganche.innerHTML = "";

    enganche.innerHTML =  (valor * 0.3) ;
    totalF.innerHTML =  (valor - enganche.innerHTML) + ((valor - enganche.innerHTML) * (planCredito));
    
    if(planCredito == 0.125){
        pagoMensu.innerHTML = totalF.innerHTML / 12; 
    }else if(planCredito == 0.172){
        pagoMensu.innerHTML = totalF.innerHTML / 18; 
    }else if(planCredito == 0.21){
        pagoMensu.innerHTML = totalF.innerHTML / 24; 
    }else if(planCredito == 0.26){
        pagoMensu.innerHTML = totalF.innerHTML / 36;
    }else if(planCredito == 0.45){
        pagoMensu.innerHTML = totalF.innerHTML / 48;
    }
}

function limpiar(){

    let valor = document.getElementById('valorAuto');
    let planCredito = document.getElementById('plan');
    let enganche = document.getElementById('pEnganche');
    let totalF = document.getElementById('pTotalFinanciar');
    let pagoMensu = document.getElementById('pagoMensual');

    pagoMensu.innerHTML = "";
    totalF.innerHTML = "";
    enganche.innerHTML = "";
}

 